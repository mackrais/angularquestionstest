import {Component, Input, OnInit} from '@angular/core';
import {MatHorizontalStepper} from '@angular/material';
import {Question} from '../../shared/models/question/question.model';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'mr-question-content',
  templateUrl: './question-content.component.html',
  styleUrls: ['./question-content.component.css'],
})
export class QuestionContentComponent implements OnInit {

  @Input() stepper: MatHorizontalStepper;
  @Input() questions: Question[];
  @Input() formList: FormGroup[];

  form: FormGroup | null;
  currentQuestion: Question | null;

  ngOnInit(): void {
    if (this.stepper && this.questions) {
      this.currentQuestion = this.questions[this.stepper.selectedIndex] || null;
      this.form = this.formList[this.stepper.selectedIndex] || null;
    }

    this.stepper.selectionChange.subscribe(
      (item: MatHorizontalStepper) => {
        this.currentQuestion = this.questions[item.selectedIndex] || null;
        this.form = this.formList[item.selectedIndex] || null;
      },
    );
  }

  loop(value: string | object): boolean {
    return typeof value === 'object';
  }
}
