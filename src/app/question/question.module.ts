import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {QuestionComponent} from './question.component';
import {QuestionRoutingModule} from './question-routing.module';
import {SharedModule} from '../shared/shared.module';
import { QuestionStepperComponent } from './question-stepper/question-stepper.component';
import {QuestionService} from '../shared/services/question/question.service';
import {UserService} from '../shared/services/user/user.services';
import {AnswerService} from '../shared/services/question/answer.service';
import { QuestionContentComponent } from './question-content/question-content.component';
import {SafePipe} from '../shared/pipes/safe.pipe';


@NgModule({
    declarations: [QuestionComponent, QuestionStepperComponent, QuestionContentComponent, SafePipe],
    imports: [
        CommonModule,
        QuestionRoutingModule,
        SharedModule
    ],
    providers: [
        QuestionService,
        UserService,
        AnswerService,
    ]
})
export class QuestionModule {
}
