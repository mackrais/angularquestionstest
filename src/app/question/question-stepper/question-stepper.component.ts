import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS, StepperSelectionEvent} from '@angular/cdk/stepper';
import {MatHorizontalStepper} from '@angular/material';
import {Answer} from '../../shared/models/question/answer.model';
import {Question} from '../../shared/models/question/question.model';
import {BaseComponent} from '../../shared/core/base-component';
import {UserAnswer} from '../../shared/models/question/user-answer.model';
import {AnswerService} from '../../shared/services/question/answer.service';
import {QuestionAnswer} from '../../shared/models/question/question-answer.model';
import {takeUntil} from 'rxjs/operators';
import {User} from '../../shared/models/user/user.model';

@Component({
  selector: 'mr-question-stepper',
  templateUrl: './question-stepper.component.html',
  styleUrls: ['./question-stepper.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class QuestionStepperComponent extends BaseComponent implements OnInit {
  @Output() setStepper = new EventEmitter<MatHorizontalStepper>();
  @ViewChild(MatHorizontalStepper, {static: true}) stepper: MatHorizontalStepper;

  @Input() user: User;
  @Input() userAnswer: UserAnswer[];
  @Input() questions: Question[];
  @Input() formList: FormGroup[];
  @Input() indexStep: number;

  currentAnswers: QuestionAnswer[];
  rightAnswers: QuestionAnswer[];

  constructor(private answerService: AnswerService) {
    super();
  }

  ngOnInit() {
    setTimeout(() => {
      this.setStepper.emit(this.stepper);
    });
    this.currentAnswers = this.findAnswer(this.stepper.selectedIndex, 'answerID');
    this.rightAnswers = this.findAnswer(this.stepper.selectedIndex, 'rightAnswer');
  }

  changeStep($event?: StepperSelectionEvent) {
    this.currentAnswers = this.findAnswer($event.selectedIndex, 'answerID');
    this.rightAnswers = this.findAnswer($event.selectedIndex, 'rightAnswer');
  }

  findAnswer(index: number, attr: string) {
    const question = this.questions[index] || null;
    if (question) {
      const userAnswer = this.answerService.findUserAnswer(question.id, this.userAnswer);
      if (userAnswer) {
        const result = question.answers.filter((answer: QuestionAnswer) => {
          return userAnswer[attr].includes(answer.id);
        });
        return result || null;
      }
    }
    return null;
  }

  submitAnswer(): void {
    const form = this.formList[this.stepper.selectedIndex] || null;
    const question = this.questions[this.stepper.selectedIndex] || null;
    const values = Object.values(form.value).filter((item) => item !== null);
    let answerIDs = [];

    if (values.length > 1) {
      question.answers.forEach((a: QuestionAnswer) => {
        const key = this.answerService.generateKey(question, a);
        if (form.get(key).value === true) {
          answerIDs.push(a.id);
        }
      });
    } else {
      answerIDs = values;
    }
    this.saveUserAnswer(question, answerIDs, form);
  }

  saveUserAnswer(question: Question, answerIDs: number[], form: FormGroup): void {
    this
      .answerService
      .getRightAnswer(question)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((data: Answer) => {
        const oldUserAnswer = this.answerService.findUserAnswer(question.id, this.userAnswer);
        const userAnswer = new UserAnswer(question.id, answerIDs, data.answerID, this.user.id);
        if (oldUserAnswer) {
          userAnswer.id = oldUserAnswer.id;
        }
        this.userAnswer.push(userAnswer);
        this
          .answerService
          .saveAnswer(userAnswer, 1)
          .pipe(takeUntil(this.unsubscribe))
          .subscribe(() => {
            form.disable();
            if (!this.answerService.checkRightAnswer(userAnswer)) {
              form.setErrors([{
                incorrectAnswer: true,
                rightAnswer: userAnswer.rightAnswer,
                questionID: question.id
              }]);
            }
            this.stepper.selected.completed = true;
            this.stepper.next();
          });
      });
  }
}
