import {Component, OnInit} from '@angular/core';
import {BaseComponent} from '../shared/core/base-component';
import {Question} from '../shared/models/question/question.model';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {QuestionService} from '../shared/services/question/question.service';
import {AnswerService} from '../shared/services/question/answer.service';
import {combineLatest} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {UserService} from '../shared/services/user/user.services';
import {User} from '../shared/models/user/user.model';
import {MatHorizontalStepper} from '@angular/material';
import {UserAnswer} from '../shared/models/question/user-answer.model';

@Component({
    selector: 'mr-question',
    templateUrl: './question.component.html',
    styleUrls: ['./question.component.css']
})
export class QuestionComponent extends BaseComponent implements OnInit {

    stepper: MatHorizontalStepper;
    opened = false;
    isLoaded = false;

    userAnswer: UserAnswer[];
    questions: Question[];
    user: User;
    formList: FormGroup[];

    constructor(
        private formBuilder: FormBuilder,
        private questionService: QuestionService,
        private userService: UserService,
        private answerService: AnswerService
    ) {
        super();
    }

    ngOnInit() {
        this.prepareData();
    }

    prepareData(): void {
        this.userService
            .getUser(1)
            .pipe(takeUntil(this.unsubscribe))
            .subscribe((user: User) => {
                this.user = user;
                combineLatest(
                    this.questionService.getQuestions().pipe(takeUntil(this.unsubscribe)),
                    this.answerService.getUserAnswer(this.user.id).pipe(takeUntil(this.unsubscribe)),
                ).subscribe((data: [Question[], UserAnswer[]]) => {
                    this.questions = data[0];
                    this.userAnswer = data[1];
                    this.prepareFormList();
                    this.isLoaded = true;
                });
            });
    }

    prepareFormList(): void {
        this.formList = [];

        this.questions.forEach(question => {
            const group: any = {};
            const names: any = {};
            const userAnswer = this.answerService.findUserAnswer(question.id, this.userAnswer);

            question.answers.forEach(answer => {
                const key = this.answerService.generateKey(question, answer);
                names[answer.id] = key;
                group[key] = new FormControl();
                if (userAnswer) {
                     group[key].disable();
                }
            });

            const formGroup = new FormGroup(group, {updateOn: 'submit'});

            if (userAnswer) {
                if (typeof userAnswer.answerID === 'object') {
                    userAnswer.answerID.forEach(id => {
                        const formControl = names[id];
                        if (formControl) {
                            group[formControl].setValue(id);
                        }
                    });
                }

                if (!this.answerService.checkRightAnswer(userAnswer)) {
                    formGroup.setErrors([{
                        incorrectAnswer: true,
                        rightAnswer: userAnswer.rightAnswer,
                        questionID: question.id
                    }]);
                }
            }

            const IdForm = this.formList.push(formGroup);
            if (userAnswer && IdForm) {
                this.formList[IdForm - 1].markAsTouched();
            }
        });
    }

    setStepper(stepper: MatHorizontalStepper) {
        this.stepper = stepper;
    }
}
