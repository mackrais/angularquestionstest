import {QuestionContentModel} from './question-content.model';
import {QuestionAnswer} from './question-answer.model';

export class Question {
    constructor(
        public id: number,
        public question: string,
        public content: QuestionContentModel,
        public type: string,
        public answers: QuestionAnswer[]
    ) {
    }
}
