export class QuestionAnswer {
    constructor(
        public id: number,
        public answer: string,
        public caption?: string
    ) {
    }
}
