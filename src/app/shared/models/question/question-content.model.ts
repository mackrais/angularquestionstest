export class QuestionContentModel {
    constructor(
        public type: string,
        public value: string | object,
    ) {
    }
}
