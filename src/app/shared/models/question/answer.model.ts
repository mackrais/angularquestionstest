export class Answer {
    constructor(
        public questionID: number,
        public answerID: number[],
    ) {
    }
}
