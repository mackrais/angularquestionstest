export class UserAnswer {
    constructor(
        public questionID: number,
        public answerID: number[],
        public rightAnswer: number[],
        public userID: number,
        public id?: number
    ) {
    }
}
