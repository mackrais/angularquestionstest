import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class BaseApi {

    constructor(protected httpClient: HttpClient) {}

    private getUrl(url: string): string {
        return `${environment.API_URL}/${url}`;
    }

    public get(url: string): Observable<any> {
        return this.httpClient.get(this.getUrl(url));
    }

    public post(url: string, data: any = {}): Observable<any> {
        return this.httpClient.post(this.getUrl(url), data);
    }

    public put(url: string, data: any = {}): Observable<any> {
        return this.httpClient.put(this.getUrl(url), data);
    }

    public patch(url: string, data: any = {}): Observable<any> {
        return this.httpClient.patch(this.getUrl(url), data);
    }

    public delete(url: string): Observable<any> {
        return this.httpClient.delete(this.getUrl(url));
    }

}
