import {Injectable, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()

export class BaseComponent implements OnDestroy {

    protected unsubscribe: Subject<void> = new Subject();

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
