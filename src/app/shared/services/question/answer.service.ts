import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseApi} from '../../core/base-api';
import {Observable} from 'rxjs';
import {Answer} from '../../models/question/answer.model';
import {Question} from '../../models/question/question.model';
import {UserAnswer} from "../../models/question/user-answer.model";
import {QuestionAnswer} from "../../models/question/question-answer.model";

@Injectable({providedIn: 'root'})
export class AnswerService extends BaseApi {

    constructor(public httpClient: HttpClient) {
        super(httpClient);
    }

    getRightAnswer(question: Question): Observable<Answer> {
        return this.get(`right-answers/question/${question.id}`);
    }

    getUserAnswer(userID: number): Observable<Answer[]> {
        return this.get(`answer/user/${userID}`);
    }

    saveAnswer(userAnswer: UserAnswer, userID: number): Observable<UserAnswer[]> {
        return this.post(`answer/user/${userID}`, userAnswer);
    }

    findUserAnswer(questionID, userAnswers: UserAnswer[]): UserAnswer | null {
        let result = null;
        if (userAnswers) {
            result = userAnswers.find(answer => {
                return answer.questionID === questionID;
            });
        }
        return result;
    }

    checkRightAnswer(answer: UserAnswer) {
        let rightAnswer = [];

        if (answer.rightAnswer instanceof Array) {
            rightAnswer = answer.rightAnswer;
        }

        if (answer.answerID instanceof Array) {
            return !answer.answerID.filter(x => !rightAnswer.includes(x)).length;
        }
        return answer.answerID === answer.rightAnswer;
    }

    generateKey(question: Question, answer: QuestionAnswer){
        let key = '';
        switch (question.type) {
            case 'checkbox':
                key = question.type + answer.id;
                break;
            case 'radio-left-image':
            case 'radio':
                key = question.type + question.id;
                break;
        }
        return key;
    }
}
