import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseApi} from '../../core/base-api';
import {Observable} from 'rxjs';
import {Question} from '../../models/question/question.model';

@Injectable({providedIn: 'root'})
export class QuestionService extends BaseApi {

    constructor(public httpClient: HttpClient) {
        super(httpClient);
    }

    getQuestions(): Observable<Question[]> {
        return this.get(`questions`);
    }
}
