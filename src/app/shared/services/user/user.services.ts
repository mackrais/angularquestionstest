import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseApi} from '../../core/base-api';
import {Observable} from 'rxjs';
import {User} from '../../models/user/user.model';

@Injectable()
export class UserService extends BaseApi {

    constructor(public httpClient: HttpClient) {
        super(httpClient);
    }

    getUser(id: number): Observable<User> {
        return this.get(`users/${id}`);
    }
}
